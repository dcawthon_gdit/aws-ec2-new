#resource "aws_instance" "myweb" {
#  ami           = "ami-0e763a959ec839f5e"
#  instance_type = "t2.micro"

#  tags = {
#    Name = "AkuDev"
#  }
#}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "myweb"

  ami                    = "ami-0cff7528ff583bf9a"
  instance_type          = "t2.micro"
 ## key_name               = "user1"
  monitoring             = true
  vpc_security_group_ids = ["sg-07e297723eda77823"]
  subnet_id              = "subnet-0e6b364d2da5981d0"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

