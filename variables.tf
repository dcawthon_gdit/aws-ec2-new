variable "access_key" {
    type         = string
    description  = "AWS Accesss Key"
}
variable "secret_key" {
    type         = string
    description  = "AWS Secret Key"
}
variable "region" {
    type         = string
    description  = "AWS Region"
    default = "us-east-1"
}
